#!/usr/bin/env bash
#
# Script is used to  generate the full runnable command line of each script of timsli  gitlab virshkvm project.
#
# 
#   
# Mandatory Arguments:
#   runnableScriptName: the name of .sh script for which this script generate a full runnable bash cli. The possible values are 'InstallRhelBasedKvmMachine.sh' 'RhelBasedKvmMachinePostInstallConfig.sh'
#
# Optional Arguments:
set -eu
set -o pipefail


# Dependencies packages
declare -a DEPENDENCIES_PACKAGES=()

# Color for consol output
export RED=$(tput setaf 1)
export GREEN=$(tput setaf 2)
export ORANGE=$(tput setaf 3)
export YELLOW=$(tput setaf 3)
export BLUE=$(tput setaf 4)
export MAGENTA=$(tput setaf 5)
export LINE=$(printf '_%.s' {1..120})
export RESET=$(tput sgr0)

## Adipappi infra bash global constant and string
export OK=true
export KO=false
export EMPTY_STR=""
export SCRIPT_NAME=$( basename -- "$BASH_SOURCE" )
export SCRIPT_DIR="$(dirname "$(realpath "${BASH_SOURCE[0]}")")"

# default parameters
declare ALL_VARS_DEFAULT_PREFIX="local_vms"
declare -a virshkvmProjectScriptNames=($(find $SCRIPT_DIR -type f -name "*.sh" | grep -v "${SCRIPT_NAME}"))

## Mandatory Arguments Array
declare -a mandatoryArgsArray=(runnableScriptName)

## Different Array or Arguments
declare -A optionalArgsArray=([bashVarsPrefix]=${ALL_VARS_DEFAULT_PREFIX} )
declare -a optionalArgsNameArray=(${!optionalArgsArray[*]})

## Build All Arguments Array
declare -a allArgNames=( ${mandatoryArgsArray[*]} ${!optionalArgsArray[*]} )
## All the arguments provided to the  script 
declare allArgsProvidedToScript="${BASH_ARGV[@]}"


##
# Return the local day in the following format "-u +%Y-%m-%dT%H:%M:%S%Z"
#
function getCurrentLocalDate(){
  date +"%Y-%m-%dT%H:%M:%S%Z"
}


#
# Print the given message in specified color on to terminal
#
# $1: Message to print into console
# $2: Output color of the message on console
#
function printInputInColor(){
  if [[ $# -ne 2 ]]; then
    printf "Usage example: ${FUNCNAME[0]} USAGE_MESSAGE \$YELLOW\n"
    exit -100
  fi
    printf "$2 $1\n$RESET"
}


##
# Check that the binary is installed otherwise  exit.
#
# Check that the binary command provided is available on the OS where this function is executed.
# if the binary is not installed stop execution.
#
#  $1: the binary linux command to check.
function checkOsBinary(){ 
if [[ $# -ne 1 ]]
  then
    printf "@FuncArgError:${FUNCNAME[0]}:Line=${BASH_LINENO} needs <runnableBinaryName>. Example: ${FUNCNAME[0]} yamllint.\n"
    exit -120
  fi
  local binaryName=$1
  if [[ $(command -v "$binaryName" 2>/dev/null) == "" ]]
  then
    printf "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} a required binary '$binaryName' is not installed on $(hostname) server. Please install it\n" 
    exit -121
  fi
}


##
#
# Check that all script dependencies package are present on the current system otherwise stop and exit. 
function checkDependencies(){
  for pkg in ${DEPENDENCIES_PACKAGES[*]}; do 
    checkOsBinary "$pkg"
  done
}


##
function usage(){
  echo "$(getScriptName) --runnableScriptName=InstallRhelBasedKvmMachine.sh --bashVarsPrefix=lustre_vms"
}


# Get script name
function getScriptName(){
  echo $( basename -- "$BASH_SOURCE" )
}


#FNCT:960
#  
# Check that bashVarsPrefix '^[a-zA-Z][0-9a-zA-Z_]*$'"
#
#
function checkWord(){
  if [[ $# -ne 1 ]]; then
    printf "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}  ${FUNCNAME[0]} wordToCheck\n"
    exit -150
  fi
  if [[ ! $1 =~ ^[a-zA-Z][0-9a-zA-Z_]* ]] 
  then
    printf "The provided string "$1" is incorrect\n"   
    exit -151
  fi
}


##
# Check than all the arguments provided to the script match the format '--argName=value'
#
function checkAllArgumentsFormat(){
  for arg in ${allArgsProvidedToScript}; do
  local argProvided=$(echo "$arg" | grep -Po "(?<=^\-\-)(\w+)=" | cut -d'=' -f1) 
  if [[  $argProvided == ${EMPTY_STR} ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}. $(getScriptName) script's argument must be probided --\w+=Value\n" $RED
    exit -170
  elif [[ ! " ${allArgNames[*]} " == *" $argProvided "*  ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}. The argument provided $arg is incorrect.\n$(getScriptName) script arguments must on of this:\n${allArgNames[*]}\n" $RED
    exit -170
  fi
  done
}


##
# Get all arguments of the script and check that all the mandatory are present
#
#
function checkMandatoryArgs(){
  for argName in ${mandatoryArgsArray[*]}; do
  if [[ ! "$allArgsProvidedToScript" == *"--$argName="* ]]; then 
      printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} The mandatory argument $argName must be probided to this script as --$argName=\n" $RED
      exit -180
    fi
  done
}


##
# Returns all the optional arguments provided to the script as --optArg1=value1 --optArg2=value2 etc.
#
#
# return: the list of all optional argument provided to the script. 
function getAllOptionalArgumentsProvided(){
  local res=""
  for optArgName in ${optionalArgsNameArray[*]}; do
  if [[ "$allArgsProvidedToScript" == *"--$optArgName="* ]]; then 
    res="$res $optArgName"
  fi
  done
  echo "${res:1}"
}


##
#
# Export the value of an argument from its name. If the argument is optional and was provided to the script, then 
# it's default value is ised as its value
#
function exportAllArguments(){
  checkMandatoryArgs
  checkAllArgumentsFormat
  for arg in ${allArgsProvidedToScript}; do
     local argName=$(echo "$arg"| cut -d'=' -f1) 
     argName=${argName:2}
     export "$argName"=$(cut -d'=' -f2 <<< $arg)
  done
  # Process the optional argument since they are not always provided as script argument
  # So it was not exported from the for loop !!!
  for optArg in ${optionalArgsNameArray[*]}; do
    if [[ $(declare -x | grep -Po "(?<=declare -x )($optArg=)") == ${EMPTY_STR} ]]; then    
      export "$optArg"=${optionalArgsArray[$optArg]}
    fi
  done
}


##
#
# Check and return the full qualified path of runnableScriptName
#
function checkRunnableScriptName(){
  if [[ ! $runnableScriptName == ${EMPTY_STR} ]]; then
    local scriptFQP="${SCRIPT_DIR}/$runnableScriptName"
    if [[ ! -f $scriptFQP ]]; then
      printInputInColor "@Info:$(getScriptName):Line:${BASH_LINENO} There is no script named '${scriptFQP}' file  on the current host $(hostaname)" $RED
      exit -120
    else
      if [[  ! " ${virshkvmProjectScriptNames[*]} " == *" $scriptFQP "* ]]; then
        printInputInColor "@Info:$(getScriptName):Line:${BASH_LINENO} There is no virshkvm's script named '${scriptFQP}' file  within project directory: ${SCRIPT_DIR}" $RED
	exit -121
      fi
    fi
  fi
  echo $scriptFQP
}


function checkBashVarsPrefix(){
 if [[ $bashVarsPrefix != "" ]]; then
    checkWord $bashVarsPrefix
 fi
}


function getRunnableScriptNameFQP(){
  echo "$(checkRunnableScriptName)"
}


##
#
#@todo Generate the full runnable bash command for the runnableScriptName
function getScriptMandatoryArguments(){
  local scriptFQP="$(getRunnableScriptNameFQP)"
  echo $(grep -Po "(?<=mandatoryArgsArray=\()([^\)]+)" $scriptFQP)
}


##
#
function getScriptOptionalArguments(){
  local scriptFQP="$(getRunnableScriptNameFQP)"
  local optArgs1=$(grep -Po "(?<=optionalArgsArray=\()([^\)]+)" $scriptFQP | grep -Po "(?<=\[)([^\]]+)")
  local optArgs2=$(grep -Po "(?<=^optionalArgsArray\[)([^\]]+)" $scriptFQP)
  echo $optArgs1 $optArgs2
}


##
#
# Generate the full runnable bash command for the runnableScriptName
# 
generateScriptCommand(){
    printInputInColor "@todo" $RED     
}


function main(){
  checkDependencies
  exportAllArguments
  checkBashVarsPrefix
  local res=$(checkRunnableScriptName)
  getScriptMandatoryArguments
  getScriptOptionalArguments
}


## Run main
main
