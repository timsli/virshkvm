#!/usr/bin/env bash
#
# Script is used to setup a RHEL 8+ Based KVM Machine On Ubuntu Host Machine. The cloud images are downloaded per default into "/caldrons/images". 
# If one wishes to customize this folder then the value of the bash variable declare in this script 'vmImagesParentFolderFQP' should be modified.  
# 
# The ns domain  used is: kvm.adipappi
# the vm hostname == vmName
#
# Arguments:
#   Mandatory: --vmName=\w+ --vcpu=\d+  --ramSizeMiB=\d{4,} --hostBridgeName=\w+ --vmUsername=[a-zA-Z]\w+  --vmGroupname=[a-zA-Z]\w+ 
#    vmName: the name of the virtual machine to create must be conformed to perl "\w+" pattern
#    vcpu: vm vcpu number
#    ramSizeMiB: vm memory size in MiB
#    vmUsername: the admin username to create into vm
#    vmGroupname: the group of admin username to create into vm 
#    osdisk1GiB: the first disk of the vm used to install vm's os image.
#    sshPublicKeyFQP: The FQP of the file containing  vmUsername's  ssh public key to deploy into the virtual machine with cloud-init.  
#
#   Optional:  --deleteIfVmExists=false --cloudImageDownloadUrl --vmPassword 
#    deleteVmIfExists: boolean indicating to delete or not the vm if it exists.
#    cloudImageDownloadUrl: the download URL of the cloud image file.
#    vmPassword: the clear password of vmUsername. By default this password is used to generated a sha-512 crypted password with mkpasswd command. 
#    vmDomainName: the domain name of the vm. Default local.adipappi
#    vmMacAddress: the mac address of the vm within network kvmNatNetworkName.
#    osdisk2GiB: the second disk of the vm used to install vm's os image.
#    osdiskConfig: The level of raid used for os disks. Per default the os     
#    createLegacyBridgeNicName: Boolean indicating to create if not exist the given hostBridgeName. 
#    hostNicName: the name of an existing host's nic used to create if necessary linux legacy bridge. Used by createBridgeNic function. 
#    hostBridgeName: the name of  host's bridge nic to create if not exist and to use by the vms.
#    kvmNatNetworkName: the name of kvn nat internal network to create with virsh define-net
#    forwardKvmNetworkToHostWifi: boolean  $OK or $KO indicating that the kvm bridge nic  traffic will be forwarded into internet
#    hostWifiNicName: the name of host's wifi nic to used when the argument "forwardKvmNetworkToHostWifi" value is "true". 
#
#
#
#  Example of creating a kvm vm with access to internet from host wifi 
#
#   1:  sudo /papi/projects/virshkvm/InstallRhelBasedKvmMachine.sh --vmName=vstrglnx1 --vcpu=2 --ramMiB=2048 --vmUsername=vadima --vmGroupname=adimida --sshPublicKeyFQP=/home/timsli/.ssh/id_ed25519.pub --deleteVmIfExists=true --hostNicName=virbr0 --kvmNatNetworkName=kvmnatbridge0  --createLegacyBridgeNicName=true --forwardKvmNetworkToHostWifi=true

#  Example of creating a kvm vm without access to internet from host wifi 
#   12:  sudo /papi/projects/virshkvm/InstallRhelBasedKvmMachine.sh --vmName=vstrglnx1 --vcpu=2 --ramMiB=2048 --vmUsername=vadima --vmGroupname=adimida --sshPublicKeyFQP=/home/timsli/.ssh/id_ed25519.pub --deleteVmIfExists=true --hostNicName=virbr0 --kvmNatNetworkName=kvmnatbridge0  --createLegacyBridgeNicName=true --forwardKvmNetworkToHostWifi=false
#
set -eu
set -o pipefail


# Get the host cpu number
function getHostCpuCount(){
  ncproc
}


# Generate sha-512 password with the default password with default mkpasswd 
# 
function generateMkpassword(){
  mkpasswd -m sha-512 "${vmPassword}"
}


##
#
# Generate randommac address for qemu nic
#
function generateMacAddress(){
  echo $(randmac -q)
}


# Color for consol output
export RED=$(tput setaf 1)
export GREEN=$(tput setaf 2)
export ORANGE=$(tput setaf 3)
export YELLOW=$(tput setaf 3)
export BLUE=$(tput setaf 4)
export MAGENTA=$(tput setaf 5)
export LINE=$(printf '_%.s' {1..120})
export RESET=$(tput sgr0)

## Adipappi infra bash global constant and string
export OK=true
export KO=false
export EMPTY_STR=""
export NOT_YET_SET="NOT_YET_SET"
export IPV4_REGEX="(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])"
export QEMU_MAC_ADDRESS_REGEX="(52:54:00:[0-9a-f][0-9a-f]:[0-9a-f][0-9a-f]:[0-9a-f][0-9a-f])"


## List of linux distribution name for the script 
declare -a VALIDE_DISTROS=(debian ubuntu mint)

## Dependencies 
# Cloud-localds is provided by cloud-utils
# mkpasswd is provided by whois
# nproc is provided by coreutils
# qemu-img is provided by qemu-utils
# qemu kvm virsh are provided by qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils 
# nmcli provided by network-manager
declare -a DEPENDENCIES_BINARY=(curl kvm virsh nproc mkpasswd qemu-img virt-install cloud-localds  dnsmasq nmcli randmac)
declare -a DEPENDENCIES_PACKAGES=(curl qemu-kvm libvirt-daemon-system libvirt-clients bridge-utils coreutils whois qemu-utils virtinst cloud-utils dnsmasq network-manager randmac)

# default parameters
declare -r PAPI_DOWNLOADS_FOLDER_FQP=/papi/downloads

# is better to provide the DEFAULT_USER_PASSWD as and env variable from protected env file.
declare -r DEFAULT_USER_PASSWD="Virtual*_1446"

declare -r DEFAULT_VM_DOMAIN_NAME="kvm.adipappi"
declare -r DNS_SERVER_1="208.67.222.222"
declare -r DNS_SERVER_2="1.1.1.1"
declare -r DNS_SERVER_3="8.8.4.4"
declare -r PUBLIC_IP_DNS_SERVERS="[${DNS_SERVER_1},${DNS_SERVER_2},${DNS_SERVER_3}]"

# Local /27 172.23.1.0 dedicated to kvm vm.
export DEFAULT_VIRTIO_NET_NAME="kvmnatbridge0"
declare -r kvmNatNetworkXmlFileFQP="/tmp/kvmNetworkDefinition.xml"
export kvmNetworkAddress=172.23.1.1
export kvmNetworkMask=255.255.255.0
export kvmNetworkDhcpStart=172.23.1.10
export kvmNetworkDhcpEnd=172.23.1.240
declare -r resolvConfFileFQP=/etc/resolv.conf
declare -r DEFAULT_VIRTIO_BRIDGE=virbr0


# Cloud image download url
declare almalinuxCloudImageURL="https://repo.almalinux.org/almalinux/9/cloud/x86_64/images/AlmaLinux-9-GenericCloud-9.3-20231113.x86_64.qcow2"
declare cloudImageDownloadDefaultUrl=${almalinuxCloudImageURL}
declare cloudImageOriginalFileName=${cloudImageDownloadDefaultUrl##*/}
declare cloudImageOriginalFileFQP=${PAPI_DOWNLOADS_FOLDER_FQP}/${cloudImageOriginalFileName}
#declare vmOsNameDefaultName=$(grep -Po "^(\w+)$" <<< $cloudImageOriginalFileName | tr 'A-Z' 'a-z')
declare vmOsNameDefaultName=almalinux8

# Cloud init files Variables used to configure cloud-init config files.
declare -r USER_DATA=user-data
declare -r META_DATA=meta-data
declare -r NETWORK_CONFIG=network-config

#  Vm specific installation files and directories
declare installationLogRootFolderFQP=/caldrons/install/logs
declare vmImagesParentFolderFQP=/caldrons/images
declare vmResourcesFQP="NOT_YET_SET" 
declare vmInstallLogFQP="NO_YET_SET"
declare vmCustomCloudInitIso="NOT_YET_SET"
declare vmCustomCloudInitFQP="NOT_YET_SET"
declare vmQcow2DiskImageName="NOT_YET_SET"
declare vmQcow2DiskImageFQP="NOT_YET_SET"

## Mandatory Arguments Array
declare -a mandatoryArgsArray=(vmName vcpu ramMiB vmUsername vmGroupname hostNicName sshPublicKeyFQP forwardKvmNetworkToHostWifi)

## Different Array or Arguments
declare -A optionalArgsArray=([deleteVmIfExists]=false [cloudImageDownloadUrl]=$cloudImageDownloadDefaultUrl [vmPassword]=${DEFAULT_USER_PASSWD} [vmDomainName]=${DEFAULT_VM_DOMAIN_NAME} [vmOsName]=$vmOsNameDefaultName)
optionalArgsArray[kvmNatNetworkName]=${DEFAULT_VIRTIO_NET_NAME}
optionalArgsArray[vmMacAddress]="00:00:00:00:00:00"
optionalArgsArray[hostBridgeName]="virbr0"
optionalArgsArray[createLegacyBridgeNicName]=$KO
optionalArgsArray[kvmNetworkAddress]=$kvmNetworkAddress
optionalArgsArray[kvmNetworkMask]=$kvmNetworkMask
optionalArgsArray[kvmNetworkDhcpStart]=$kvmNetworkDhcpStart
optionalArgsArray[kvmNetworkDhcpEnd]=$kvmNetworkDhcpEnd
optionalArgsArray[forwardKvmNetworkToHostWifi]=$KO
optionalArgsArray[hostWifiNicName]=wlp61s0
declare -a optionalArgsNameArray=(${!optionalArgsArray[*]})
 
## Build All Arguments Array
declare -a allArgNames=( ${mandatoryArgsArray[*]} ${!optionalArgsArray[*]} )
## All the arguments provided to the  script 
declare allArgsProvidedToScript="${BASH_ARGV[@]}"


##
# Return the local day in the following format "-u +%Y-%m-%dT%H:%M:%S%Z"
#
function getCurrentLocalDate(){
  date +"%Y-%m-%dT%H:%M:%S%Z"
}

#
# Print the given message in specified color on to terminal
#
# $1: Message to print into console
# $2: Output color of the message on console
#
function printInputInColor(){
  if [[ $# -ne 2 ]]; then
    printf "Usage example: ${FUNCNAME[0]} USAGE_MESSAGE \$YELLOW\n"
    exit -100
  fi
    printf "$2 $1\n$RESET"
}


##
#
# return in lowercase the current server distribution name.  
#
# @return: lowercased distro name
function getDistroLowercaseName(){
  echo $(grep -Po '(?<=^ID=)([\S]+)' /etc/*-release  | cut -d':' -f2 | tr '[:upper:]' '[:lower:]' |sort -u)
}


##
#
# Exit with an error code is the distro name lowercased is not element of VALIDE_DISTROS array.
#
function checkDistro(){
  local distroName=$(getDistroLowercaseName)
  if [[ ! " ${VALIDE_DISTROS[*]} " == *" $distroName "* ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} This script must be executed only on ${VALIDE_DISTROS[*]} distro.\nCurrent distro is: $distroName \n" $RED
    exit -110
  fi
}


##
# Check that the binary is installed otherwise  exit.
#
# Check that the binary command provided is available on the OS where this function is executed.
# if the binary is not installed stop execution.
#
#  $1: the binary linux command to check.
function checkOsBinary(){ 
if [[ $# -ne 1 ]]
  then
    printf "@FuncArgError:${FUNCNAME[0]}:Line=${BASH_LINENO} needs <runnableBinaryName>. Example: ${FUNCNAME[0]} yamllint.\n"
    exit -120
  fi
  local binaryName=$1
  if [[ $(command -v "$binaryName" 2>/dev/null) == "" ]]
  then
    printf "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} a required binary '$binaryName' is not installed on $(hostname) server. Please install it\n" 
    exit -121
  fi
}


##
#
# Check that all script dependencies package are present on the current system otherwise stop and exit. 
function checkDependencies(){
  for pkg in ${DEPENDENCIES_PACKAGES[*]}; do 
    if [[ $(dpkg-query -W "$pkg" 2>/dev/null) == "${EMPTY_STR}" ]];  then
      printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} a required package '$pkg' is not installed on $(hostname) server.Please install it." $ORANGE 
    fi
  done
}


## Script Usage
function usage(){
  printInputInColor "sudo $(getScriptName) --vmName=vstrglnx1 --vcpu=2 --ramMiB=2048 --vmUsername=vadima --vmGroupname=adimida  --sshPublicKeyFQP=/home/timsli/.ssh/id_ed25519.pub --hostNicName=eth1 --forwardKvmNetworkToHostWifi=true  [--optionalArgName=value]." $GREEN
  local opts="optional arguments name and default value are"
  for opt in ${!optionalArgsArray[@]}
  do
   opts="$opts [--$opt=${optionalArgsArray[$opt]}]"
  done
  printInputInColor "$opts" $ORANGE
}


# Make sure only root can run our script
function checkUserIsRootOrSudoer(){
  if [[ $EUID -ne 0 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} This script must be run as root\n" $RED
    exit -130
  fi
}


# Get script name
function getScriptName(){
  echo "$(readlink -f ${BASH_SOURCE[0]})"
}


##
#  
# Check that the word provided is compatible with perl word regex "export WORD_REGEX='^[0-9a-zA-Z_]+$'"
#
#
function checkWord(){
  if [[ $# -ne 1 ]]; then
    printf "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}  ${FUNCNAME[0]} wordToCheck\n"
    exit -150
  fi
  if [[ ! $1 =~ '^[0-9a-zA-Z_]+$' ]] 
  then
    printf "The provided string $1 is not a perl compatible word\n"   
    exit -151
  fi
}


##
# Chck the given macaddress by regex ${QEMU_MAC_ADDRESS_REGEX}
#
#
function checkMacAddress(){
  if [[ $# -ne 1 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} ${FUNCNAME[0]} function needs as argument a macAddress to checl" $RED
    exit -160
  fi
  local macAddress=$1
  if [[ ! "$macAddress" =~ ${QEMU_MAC_ADDRESS_REGEX} ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} Incorrect Qemu mac address '$macAddress'" $RED
    exit -163
  fi
}


##
# Check than all the arguments provided to the script match the format '--argName=value'
#
function checkAllArgumentsFormat(){
  for arg in ${allArgsProvidedToScript}; do
  local argProvided=$(echo "$arg" | grep -Po "(?<=^\-\-)(\w+)=" | cut -d'=' -f1) 
  if [[  $argProvided == ${EMPTY_STR} ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}. $(getScriptName) script's argument must be probided --\w+=Value\n" $RED
    exit -170
  elif [[ ! " ${allArgNames[*]} " == *" $argProvided "*  ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}. The argument provided $arg is incorrect.\n$(getScriptName) script arguments must on of this:\n${allArgNames[*]}\n" $RED
    exit -170
  fi
  done
}


##
# Get all arguments of the script and check that all the mandatory are present
#
#
function checkMandatoryArgs(){
  for argName in ${mandatoryArgsArray[*]}; do
  if [[ ! "$allArgsProvidedToScript" == *"--$argName="* ]]; then 
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} The mandatory argument $argName must be probided to this script as --$argName=<argValue>\n" $RED
    usage
      exit -180
    fi
  done
}


##
# Returns all the optional arguments provided to the script as --optArg1=value1 --optArg2=value2 etc.
#
#
# return: the list of all optional argument provided to the script. 
function getAllOptionalArgumentsProvided(){
  local res=""
  for optArgName in ${optionalArgsNameArray[*]}; do
  if [[ "$allArgsProvidedToScript" == *"--$optArgName="* ]]; then 
    res="$res $optArgName"
  fi
  done
  echo "${res:1}"
}


##
#
# Export the value of an argument from its name. If the argument is optional and was provided to the script, then 
# it's default value is ised as its value
#
function exportAllArguments(){
  for arg in ${allArgsProvidedToScript}; do
     local argName=$(echo "$arg"| cut -d'=' -f1) 
     argName=${argName:2}
     export "$argName"=$(cut -d'=' -f2 <<< $arg)
  done
  # Process the optional argument since they are not always provided as script argument
  # So it was not exported from the for loop !!!
  for optArg in ${optionalArgsNameArray[*]}; do
    if [[ $(declare -x | grep -Po "(?<=declare -x )($optArg=)") == ${EMPTY_STR} ]]; then    
      export "$optArg"=${optionalArgsArray[$optArg]}
    fi
  done
}


##
# Check vmName against  '^[a-zA-Z][\w\-]+$'
function checkVmName(){
  if [ -z "$vmName" ]; then
    printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} VM name must be provided. \n" $RED
    exit -200
  fi
  if [[ $(echo $vmName | grep -P '^[a-zA-Z][\w\-]+$') == ${EMPTY_STR} ]]; then
    printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} the name of the vm provided: '$vmName' is not a valid vm name. It should be \w+ compatible\n" $RED
    exit -209
  fi
}


##
#
# Check vmDomainName against '(?=^.{4,253}$)(^(?:[a-zA-Z](?:(?:[a-zA-Z0-9\-]){0,61}[a-zA-Z])?\.)+[a-zA-Z]{2,}$)' 
function checkVmDomainName(){
  if [[ $(echo $vmDomainName | grep -P '(?=^.{4,253}$)(^(?:[a-zA-Z](?:(?:[a-zA-Z0-9\-]){0,61}[a-zA-Z])?\.)+[a-zA-Z]{2,}$)' 2>/dev/null) == ${EMPTY_STR} ]]; then
    printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} Incorrect Domain Name $vmDomainName provided for the vm $vmName to create.\n"  $RED
  fi
}

##
# Check that the forwardKvmNetworkToHostWifi's value  is "true" or "false"
function checkForwardKvmNetworkToHostWifi(){
  if [[ ! $forwardKvmNetworkToHostWifi == $OK ]] && [[ ! $forwardKvmNetworkToHostWifi == $KO ]]; then
    printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} Script'a argument forwardKvmNetworkToHostWifi must take '$OK' or '$OK' values." $RED
  fi
}


# return OK variable's value if the vm exist with virtsh and KO variables's value otherwise
function isVmExists(){
  local res=$KO
  local allNames="$(virsh list --all --name 2>/dev/null)"
  if [[ " $allNames " == *" ${vmName} "* ]]; then
    res=$OK
  fi 
  echo "$res"
}


##
# Delete the folder containing all the resources of the vm if it exists. 
#
# This function must called only  by the deleteVm function.
function deleteVmResourcesFolder(){
  local vmFolderToDelete="${vmImagesParentFolderFQP}/${vmName}" 
  if [[ -d $vmFolderToDelete ]]; then
    printInputInColor "@Info:${FUNCNAME[0]}:Line=${BASH_LINENO} Deleting  vm '$vmName' resource folder $vmFolderToDelete." $ORANGE
    rm -rf ${vmFolderToDelete} 2> /dev/null 
  fi
}

function shutdownVm(){
  if [[ " $(virsh list --state-running --name | xargs) " == *" $vmName "* ]] ; then 
     virsh shutdown $vmName &>/dev/null 
  fi
}
## 
# Validate VM deletion if it exists and deleteVmIfExists argument  is set to true.
function deleteVm(){
  if [[ $(isVmExists) == $OK ]] && [[ $deleteVmIfExists == $OK ]] ; then
    printInputInColor "@Info:${FUNCNAME[0]}:Line=${BASH_LINENO} Deleting  vm '$vmName'." $BLUE
    shutdownVm
    virsh undefine $vmName  --remove-all-storage  2>/dev/null
    deleteVmResourcesFolder
  elif [[ $(isVmExists) == $OK ]] && [[ $deleteVmIfExists == $KO ]] ; then
    printInputInColor "@Warning:${FUNCNAME[0]}:Line=${BASH_LINENO} There is an existing kvm domain machine named '$vmName' on the '$(hostname)' host." $ORANGE
    printInputInColor "@Info:${FUNCNAME[0]}:Line=${BASH_LINENO} Run $(getScriptName) with argument deleteVmIfExists=true to delete the existing kvm domaine machine $vmName" $GREEN
  elif [[ $(isVmExists) == $KO ]] && [[ $deleteVmIfExists == $OK ]] ; then
    printInputInColor "@Info:${FUNCNAME[0]}:Line=${BASH_LINENO} There is no existing kvm domain machine named '$vmName' to delete on the '$(hostname)' host." $BLUE
    printInputInColor "@Warning:${FUNCNAME[0]}:Line=${BASH_LINENO} Delete old '$vmName' vm disk image $vmQcow2DiskImageFQP on the '$(hostname -f)' host." $ORANGE
    rm -rf ${vmQcow2DiskImageFQP} 2>/dev/null
  fi
}


##
# Validate ssh public key
function checkSshPublicKey(){
  if [[ $(ssh-keygen -l -f "$sshPublicKeyFQP" 2> /dev/null) == ${EMPTY_STR} ]]; then
    printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} the file $sshPublicKeyFQP does not contain a valid ed25519 public key.\n" $RED
    exit -210
  fi
}


##
# Function checking that the given nic realy exists on the current host.
# 
# $1: name of network interfces to check
function checkNicName(){
  if [[ $# -ne 1 ]]; then
    printf "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} ${FUNCNAME[0]} This function needs as argument the nicName to check\n"
  fi
  local found=$(ip link show dev $1 2>/dev/null)
  if [[ "$found" == "${EMPTY_STR}" ]]; then
    printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} there is no nic named '$1' on the current host '$(hostname)'." $ORANGE
    exit -220
  fi
}


## Function checking that the given nic really exists on the current host and it is  wireless nic.
# 
# $1: name of wifi network interfces to check
function checkWifiNicName(){
  if [[ $# -ne 1 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} ${FUNCNAME[0]} This function needs as argument the a wifi nic name to check.\n" $RED
  fi
  local wifiNicName=$1
  local found=$(find -H /sys/class/net/* -name wireless | cut -d / -f 5)
  if [[ ! " $found " == " $wifiNicName " ]]; then
    printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} there is no wifi nic named '$wifiNicName' on the current host '$(hostname -f)'." $ORANGE
    exit -230
  fi
}


## Function checking if the bridge nic hostBridgeName exits on the current host
# 
function isHostBridgeNameExists(){
  local res=$OK
  local bridgeNameFound=$(grep -Eo "^INTERFACE=(\w+)" /sys/class/net/${hostBridgeName}/* 2>/dev/null| cut -d '=' -f2)
  if [[ "$bridgeNameFound" != "${hostBridgeName}" ]]; then
   res=$KO
  fi
  echo "$res"
}


##
#
# Get the macaddress of the provided existing nic
function getNicMacAddress(){
  if [[ $# -ne 1 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}  ${FUNCNAME[0]} This function needs as argument the nicName.\n" $RED
    exit -230
  fi
  local nicName=$1
  checkNicName $nicName
  echo "$(ip link show virbr0 | grep link/ether | awk '{print $2}')"
}

# 
# Function to call absolutely for preparing some required files folders used by this script. 
# It must be called after script's arguments and dependencies checks.
# Export some necessary variables.
#
function prepareFilesSystem(){
  vmResourcesFQP="$vmImagesParentFolderFQP/${vmName}"
  vmInstallLogFQP="${installationLogRootFolderFQP}/${vmName}_$(getCurrentLocalDate).log"
  vmCustomCloudInitIso=${vmName}-custom-ci-data.iso
  vmCustomCloudInitIsoFQP=${vmResourcesFQP}/${vmCustomCloudInitIso}
  vmQcow2DiskImageName=${vmName}.qcow2
  vmQcow2DiskImageFQP=${vmResourcesFQP}/${vmName}.qcow2
  vmMacAddress=$(generateMacAddress)
  mkdir -p ${installationLogRootFolderFQP}
  mkdir -p $vmResourcesFQP
  mkdir -p ${PAPI_DOWNLOADS_FOLDER_FQP}
  touch $vmInstallLogFQP
}


##
#
# Download the cloud-init qcow2 image file from the cloudImageDownloadUrl optional argument value.
# If the file already exists and it is  qcow2 file skip download step.
# If one aim to download the remote image file delete the local one.
#:w
function downloadCloudImageFile(){
  if [[ $( curl --head --silent  -Iw '%{http_code}'  "$cloudImageDownloadUrl" -o /dev/null) == 200 ]]; then
    if [[ -e ${cloudImageOriginalFileFQP} ]]; then
        checkQcow2ImageFile 
    else
      cd ${PAPI_DOWNLOADS_FOLDER_FQP}
      curl -O $cloudImageDownloadUrl
    fi
  fi
}


# Validate image file
function checkQcow2ImageFile(){
  if [[ ! -e ${cloudImageOriginalFileFQP} ]]; then 
    printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} the cloud image $cloudImageOriginalFileFQP do not exist on current $(hostname) host.\n" $RED
    exit -220
  fi
  if [[ $(qemu-img check "$cloudImageOriginalFileFQP" 2> /dev/null) == ${EMPTY_STR} ]]; then  
    printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} the cloud image $cloudImageOriginalFileFQP downloaded from ${cloudImageDownloadUrl} is not a valid qcow2 file.\n" $RED
    exit -221
  fi
}


## Copy cloud-image file as vm initial image file.
function copyCloudImageToVmResourcesFolder(){
  printInputInColor "@Info:Line=${BASH_LINENO} $(getCurrentLocalDate) Copy cloud-init image downloaded qcow2 file:\n$cloudImageOriginalFileFQP to:$vmQcow2DiskImageFQP" $BLUE
  cp  "$cloudImageOriginalFileFQP" "$vmQcow2DiskImageFQP"
}


##
#
# Function printing as {"prop1: value1\n", "prop2: value2\n",...} the properties of  kvm domain vm to create.
#
function prettyPrintDomainProperties(){
   local res="Kvm Domain Properties:"
   local _allArgsNames=${allArgNames[*]}
   for arg in ${_allArgsNames//vmPassword/}; do 
     res="$res\n  $arg: ${!arg}"
    done
   echo -e "$res"
}


##
# Create Linux Standard Bridge Nic.
#
# Mandatory Args:
#   $1: the name of the host's bridge to create 
#   $2: the name of host's nic 
#
function createLegacyBridgeNic(){
  if [[ $# -ne 2 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} function ${FUNCNAME[0]} needs <hostBridgeName> <hostNicName>.\nUsage:${FUNCNAME[0]} virbr0 wlp61s1" $RED
    exit -290
  fi
  local _hostBridge=$1
  local _hostNic=$2
  checkNicName $_hostNic
  if [[ $(isHostBridgeNameExists $_hostBridge) ==  $KO ]] && [[ $createLegacyBridgeNicName == $OK ]]; then 
    printInputInColor "@Info:Line=${BASH_LINENO} $(getCurrentLocalDate) create bridge nic used to create kvm vm network." $BLUE
    nmcli con add ifname $_hostBridge type bridge con-name $_hostBridge
    nmcli con add type bridge-slave ifname $_hostNic master $_hostBridge
    nmcli con modify $_hostBridge bridge.stp no
    nmcli -f bridge con show $_hostBridge
    nmcli con down $_hostNic
    nmcli con up $_hostBridge
  fi
  printInputInColor "@Warning:Line=${BASH_LINENO} $(getCurrentLocalDate) Chekc that $_hostBridge was been created on the current host $(hostname)." $BLUE
  checkNicName $_hostBridge
}


function populateCloudInitUserDataFile(){
  ## Variables used to configure cloud-init config files.
  local sshPublicKey=$(cat $sshPublicKeyFQP)

 ## cloud-init config: set hostname, remove cloud-init package and add ssh-key
cd $vmResourcesFQP
cat > ${USER_DATA} <<_EOF_
#cloud-config
## Hostname management
preserve_hostname: False
hostname: $vmName
fqdn: $vmName.${vmDomainName}

# Set root and vmUsername  users data
users:
  - default
  - name: root
    password: $vmPassword
    ssh-authorized-keys:
      - $sshPublicKey
    lock_passwd: false
  - name: $vmUsername
    password: $vmPassword
    ssh-authorized-keys:
      - $sshPublicKey
    sudo: ALL=(ALL) NOPASSWD:ALL
    lock_passwd: false
    ssh_pwauth: true
    shell: /bin/bash

# Install some extra packages
packages:
  - epel-release
  - telnet
  - nmap

# Upgrade system
package_upgrade: true

# Remove cloud-init when finished with it
runcmd:
  -  rm -rf /var/lib/cloud/* 
  -  rm -rf $installationLogRootFolderFQP 

# Configure where output will go
output:
  all: ">> $vmInstallLogFQP"
_EOF_
}


# Generate META_DATA file 
function populateCloudInitMetaDataFile(){
  cd $vmResourcesFQP
  echo "instance-id: $vmName; local-hostname: $vmName" > ${META_DATA}
}


# Create CD-ROM ISO with  network-config and cloud-init user-data  and meta-data files
function createCloudInitCdRom(){
  printInputInColor "@Info:Line=${BASH_LINENO} $(getCurrentLocalDate) Create a iso file with cloud-init config generated file." $BLUE
  cd $vmResourcesFQP
  cloud-localds -v $vmCustomCloudInitIsoFQP ${USER_DATA} ${META_DATA} 
}


##
#
# Check is the libvirt network provided exist
#
#  $1: the name of libvirt network to check
function isKvmNetworkExists(){
  if [[ $# -ne 1 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} function ${FUNCNAME[0]} needs the name of libvirt network  to check as argument.\n" $RED
    exit -240
  fi
  local res=$OK
  if  [[ $(virsh net-info --network "$1"  2>/dev/null) == $EMPTY_STR ]]; then res=$KO; fi
  echo "$res"
}

##
# Check if the kvm network exists and it is activated.
#
#  $1: the name of libvirt network to test
#
#@return KO if the network does not exist or is not activated. 
function isKvmNetworkActive(){
  if [[ $# -ne 1 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} function ${FUNCNAME[0]} needs the name of libvirt network  to check as argument.\n" $RED
    exit -250
  fi
  local res=$OK
  local kvmNetworkName="$1"
  if  [[ $(virsh net-info --network $kvmNetworkName  2>/dev/null) == $EMPTY_STR ]] || [[ $(virsh net-info --network $kvmNetworkName  | grep -E "Active:\s+yes" 2>/dev/null) == ${EMPTY_STR} ]]; then
    res=$KO
  fi
  echo $res
}


## 
# Create a forwarder traffic between kvm's guest and internet by using host wifi nic. 
#
#  $1: the name of host wireless nic to forward to kvm default bridge virbr0 for input traffic
#  $2: the name of kvm default nic from which outgoing traffic is forwarded to host wireless nic. 
# `
function forwardTrafficBetweenKvmBrideAndHostWirelessNic(){
  if [[ $# -ne 2 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} function ${FUNCNAME[0]} needs the name of kvm bridge nic name and host wifi nic name.\nUsage:${FUNCNAME[0]} virbr0 wlp61s1" $RED
    exit -250
  fi
  local kvmBridgeNicName=$1
  local hostWifiNicName=$2
  if [[ $(isHostBridgeNameExists $kvmBridgeNicName) ==  $KO ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} function ${FUNCNAME[0]} There is no kvm bridge named '$kvmBridgeNicName' on the current kvm host: $(hostname -f)." $RED
    exit -253
  fi
  checkWifiNicName $hostWifiNicName

  # Activate globally forwaring
  echo 1 > /proc/sys/net/ipv4/ip_forward

  if [[ $forwardKvmNetworkToHostWifi ==  $OK ]]; then 
    printInputInColor "Info@${FUNCNAME[0]}:Line=${BASH_LINENO} forward the kvm bridge named '$kvmBridgeNicName' traffic to host wifi nic named: $hostWifiNicName" $RED
    # Create forwarding
    iptables -A FORWARD -i $kvmBridgeNicName -o $hostWifiNicName -j ACCEPT
    iptables -A FORWARD -i $hostWifiNicName -o $kvmBridgeNicName -m state --state ESTABLISHED,RELATED -j ACCEPT
    iptables -t nat -A POSTROUTING -o $kvmBridgeNicName -j MASQUERADE
  fi
  # local vmIp=$(getVmIpAddressFromNetworkAndMacAddress $kvmNatNetworkName $vmMacAddress)
  # vmIp=$(grep -Eo "${IPV4_REGEX}" <<< $vmIp)
  # ssh -t ${vmUsername}@${vmIp} -c "echo 'ip route add default via $kvmNetworkAddress' | sudo tee /etc/sysconfig/network-scripts/route-eth0; sudo systemctl restart network"
  # ssh -tt -o StrictHostKeyChecking=no ${vmUsername}@${vmIp} "sudo kill -9 \$(ps -def | awk '/\/usr\/bin\/dnf/ {print \$3}')"
  # ssh -tt  -o StrictHostKeyChecking=no ${vmUsername}@${vmIp} "echo -e 'nameserver 1.1.1.1\nnameserver 8.8.4.4' | sudo tee $resolvConfFileFQP; sudo chattr +i $resolvConfFileFQP"
}


##
#
#  $1: the name of libvirt network to create. 
# The parameter of this network are: 
# kvmNetworkName kvmNetworkAddress kvmNetworkMask kvmNetworkDhcpStart kvmNetworkDhcpEnd
function createKvmInternalNatNetwork(){
  if [[ $# -ne 1 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} function ${FUNCNAME[0]} needs the name of libvirt network  to create.\n" $RED
    exit -250
  fi
  local kvmNetworkName="$1"
  if [[ $(isKvmNetworkExists $kvmNetworkName) == $KO ]]; then
cat > "$kvmNatNetworkXmlFileFQP" <<_EOF
<network>
  <name>$kvmNetworkName</name>
  <uuid>$(uuidgen)</uuid>
  <forward mode='nat'/>
  <bridge name='$kvmNetworkName' stp='on' delay='0'/>
  <mac address='$(generateMacAddress)'/>
  <ip address='$kvmNetworkAddress' netmask='$kvmNetworkMask'>
    <dhcp>
      <range start='$kvmNetworkDhcpStart' end='$kvmNetworkDhcpEnd'/>
    </dhcp>
  </ip>
</network>
_EOF
    virsh net-define $kvmNatNetworkXmlFileFQP
  else
    printInputInColor "Warning@${FUNCNAME[0]}:Line=${BASH_LINENO} The kvm network $kvmNetworkName exists already on the current host $(hostname -f) .\n" $RED
  fi
  virsh net-autostart $kvmNetworkName  2> /dev/null
  if [[ $(isKvmNetworkActive  $kvmNetworkName) == $KO ]]; then 
    virsh net-start $kvmNetworkName  2> /dev/null
  fi
}


# Eject cdrom from vm
# @todo validate
function ejectCloudInitCdRom(){
  printInputInColor "@Info:Line=${BASH_LINENO} $(getCurrentLocalDate) Eject the cdrom from vm" $BLUE
  virsh change-media "$vmName" hda --eject --config >> "$vmInstallLogFQP"
}


# Create a vm with virt-install by importing the qcow2 file  and using cloud-init configuration files.
function createVirtualMachine(){
  printInputInColor "@Info:Line=${BASH_LINENO} $(getCurrentLocalDate) Install and Configure kvm domain virual machine:\n$(prettyPrintDomainProperties)" $BLUE
  local cmd="virt-install --import --name $vmName --memory $ramMiB --vcpus $vcpu --disk $vmQcow2DiskImageFQP,format=qcow2,bus=virtio --cdrom $vmCustomCloudInitIsoFQP"
  cmd="$cmd --network network=$kvmNatNetworkName,model=virtio,mac=$vmMacAddress --os-variant $vmOsName --noautoconsole"
  cd $vmResourcesFQP
  eval "$cmd"
  #until [[ $(virsh domiflist --domain "$vmName" 2>/dev/null | grep -Eo "${QEMU_MAC_ADDRESS_REGEX}") != ${EMPTY_STR} ]] ; do
  until [[ $(getVmIpAddressFromNetworkAndMacAddress $kvmNatNetworkName $vmMacAddress) != "[]" ]] ; do
    sleep 5
  done
}


# Remove the unnecessary cloud init files
function cleanCloudInitInstallationData(){
  printInputInColor "@Warning:Line=${BASH_LINENO} $(getCurrentLocalDate) Clean the cloud-init install data from $vmResourcesFQP" $BLUE
  cd $vmResourcesFQP
  rm -rf  ${USER_DATA} "$vmCustomCloudInitIso" "${NETWORK_CONFIG}" "${META_DATA}"  2> /dev/null
}


##
# Get the name of all kvm networks
#
function getAllKvmNetworkName(){
  echo $(virsh net-list --name | xargs)
}


##
#
# Check if the kvm network exists or not.
#
#
# $1: name of kvm network to check.
#
#@return  nothing. Exit if the kvm network does not exist.
function checkKvmNetwork(){
  if [[ $# -ne 1 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} function ${FUNCNAME[0]} needs <kvmNetworkName>.\n" $RED
    exit -260
  fi
  local kvmNetwork=$1
  if [[ ! " $(getAllKvmNetworkName) " == *" $kvmNetwork "* ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} There is no kvm network named $kvmNetwork on current $(hostname -f) host." $ORANGE
    exit -263
  fi
}


##
#
# Get all mac addresses for the given vm. 
#
function getVmMacAddresses(){
  echo $(virsh dumpxml "$vmName" | awk -F\' '/mac address/ {print $2}')
}


##
# return the ip address of kvm's vm  from its network and mac address.
#
#  $1: the name of kv network 
#  $2: the mac address of kvm's vm nic in kvm network.
#
# @return the ip address of the vm.
function getVmIpAddressFromNetworkAndMacAddress(){
  if [[ $# -ne 2 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} function ${FUNCNAME[0]} needs <kvmNetworkName> <vmMacAddress>\n" $RED
    exit -260
  fi
  local kvmNetwork=$1
  local vmNicMacAddress=$2
  checkKvmNetwork $kvmNetwork
  checkMacAddress $vmNicMacAddress
  local res=$(virsh  net-dhcp-leases $kvmNetwork $vmNicMacAddress | tail -n+3 | xargs | cut -d' ' -f5)
  echo "[${res// /,}]"
}


## Get with virtsh the mac and ip address of the vm.
function configureGetVmInformations(){
  local vmIp=$(getVmIpAddressFromNetworkAndMacAddress $kvmNatNetworkName $vmMacAddress)
  vmIp=$(grep -Eo "${IPV4_REGEX}" <<< $vmIp)
  printInputInColor "Info@${FUNCNAME[0]}:Line=${BASH_LINENO} Vm $vmName ip=$vmIp" $GREEN 
  #virsh net-update --network $kvmNatNetworkName add ip-dhcp-host "<host mac='$vmMacAddress' name='$vmName' ip='$vmIp'/>" --live --config 
  prettyPrintDomainProperties
  echo "vmHwAddr:$vmMacAddress"
  echo "vmIpAddrs:$vmIp"
}


function waitForValidSshConnection(){
  local vmIp=$(getVmIpAddressFromNetworkAndMacAddress $kvmNatNetworkName $vmMacAddress)
  vmIp=$(grep -Eo "${IPV4_REGEX}" <<< $vmIp)
  local res=$(ssh -t -q  -o StrictHostKeyChecking=no -o BatchMode=yes $vmUsername@${vmIp} 'exit 0')
  printInputInColor "Info@${FUNCNAME[0]}:Line=${BASH_LINENO} Ssh check on $vmUsername@$vmIp=>$res" $GREEN
  while [[ $res -ne 0 ]]; do
    res=$(ssh -t -q -o StrictHostKeyChecking=no -o BatchMode=yes $vmUsername@${vmIp} 'exit 0')
    printInputInColor "Info@${FUNCNAME[0]}:Line=${BASH_LINENO} Ssh check on $vmUsername@$vmIp=>$res" $GREEN
    sleep 10
  done
}

##
#
# Function used to call all others chercker functions. 
#
function checkScriptDataExportArguments(){
  checkUserIsRootOrSudoer 
  checkDistro
  checkDependencies
  checkAllArgumentsFormat
  checkMandatoryArgs
  exportAllArguments
  checkSshPublicKey
  checkVmName
  checkVmDomainName
}


function configureInstallCloudInitVirtualMachine(){
## Cloud-Init: begin
  downloadCloudImageFile
  checkQcow2ImageFile
  copyCloudImageToVmResourcesFolder
  populateCloudInitUserDataFile
  populateCloudInitMetaDataFile
  createCloudInitCdRom
  createVirtualMachine
  cleanCloudInitInstallationData
}


function main(){
  checkScriptDataExportArguments
  createLegacyBridgeNic $hostBridgeName $hostNicName
  createKvmInternalNatNetwork "$kvmNatNetworkName"
  deleteVm
  prepareFilesSystem
  configureInstallCloudInitVirtualMachine
  configureGetVmInformations
  waitForValidSshConnection
  forwardTrafficBetweenKvmBrideAndHostWirelessNic $kvmNatNetworkName $hostWifiNicName
}


## Run main
main

