#!/usr/bin/env bash
# Script is used to parse the VmsDefinition.yml representing the definition and properties of server infrastructure used to set Lustre Kvm Installation
#
# All the entries within VmsDefinition.yml are parsed and turned into bash variables.For each yaml tree Parent key is linked to its children's key by 
# parentKey_childrenKey and repetead again until the last yaml leaf of the tree. 
#
# Mandatory Arguments:
#   
#
# Optional Arguments:
#  vmsDefinitionFile: The yaml file named VmsDefinition.yml
#  bashVarsPrefix: the perl conpatiable "[a-zA-Z]\w+" word used to prefix all the bash variables created by this script. defaults value if empty.
#  checkFile: boolean (true|false) indicating to check the yaml file VmDefinitionFile.yml should be checked. Default value is true.
#
# Each script or function which needs the data from the Vm Definition File must call this script as: 
# res=$(/papi/projects/virshkvm/ParseVmDefinitionFile.sh  --checkFile=true --bashVarsPrefix="kvm" | grep "kvm_") 
# One can see the output of /papi/projects/virshkvm/ParseVmDefinitionFile.sh is filter by the value of bashVarsPrefix to ensure that 
# res will contain only the exported variables from VmsDefinitionFile yaml file. User should choose appropriate value for bashVarsPrefix
# to avoid any existing prefix variable from overall bash variables.
#
#
# @todo add function to extract vcpu ramMiB adminUsername and adminGroupname
# @todo add ip1 and ip2 address checker
# @todo check the mask  which must be /0 /1 ... /32
#
set -eu
set -o pipefail


# Dependencies packages
declare -a DEPENDENCIES_PACKAGES=(yamllint)


# Color for consol output
export RED=$(tput setaf 1)
export GREEN=$(tput setaf 2)
export ORANGE=$(tput setaf 3)
export YELLOW=$(tput setaf 3)
export BLUE=$(tput setaf 4)
export MAGENTA=$(tput setaf 5)
export LINE=$(printf '_%.s' {1..120})
export RESET=$(tput sgr0)

## Adipappi infra bash global constant and string
export OK=true
export KO=false
export EMPTY_STR=""
export IPV4_REGEX="(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])"
export IPV4_MASK="([0-9]|((1|2)[0-9])|(3[0-2]))"
export QEMU_MAC_ADDRESS_REGEX="(52:54:00:[0-9a-f][0-9a-f]:[0-9a-f][0-9a-f]:[0-9a-f][0-9a-f])"

# default parameters
declare -r PAPI_DOWNLOADS_FOLDER_FQP=/papi/downloads
declare VM_DEFINTION_DEFAULT_FILE_NAME="VmsDefinitionFile.yml"
declare VM_DEFINTION_DEFAULT_FILE_FQP="./VmsDefinitionFile.yml"
declare ALL_VARS_DEFAULT_PREFIX="local_vms"


## Mandatory Arguments Array
declare -a mandatoryArgsArray=()

## Different Array or Arguments
declare -A optionalArgsArray=( [vmsDefinitionFileFQP]=${VM_DEFINTION_DEFAULT_FILE_FQP} [checkFile]=$OK  [bashVarsPrefix]=${ALL_VARS_DEFAULT_PREFIX} )
declare -a optionalArgsNameArray=(${!optionalArgsArray[*]})

## Build All Arguments Array
declare -a allArgNames=( ${mandatoryArgsArray[*]} ${!optionalArgsArray[*]} )
## All the arguments provided to the  script 
declare allArgsProvidedToScript="${BASH_ARGV[@]}"


##
# Return the local day in the following format "-u +%Y-%m-%dT%H:%M:%S%Z"
#
function getCurrentLocalDate(){
  date +"%Y-%m-%dT%H:%M:%S%Z"
}


#
# Print the given message in specified color on to terminal
#
# $1: Message to print into console
# $2: Output color of the message on console
#
function printInputInColor(){
  if [[ $# -ne 2 ]]; then
    printf "Usage example: ${FUNCNAME[0]} USAGE_MESSAGE \$YELLOW\n"
    exit -100
  fi
    printf "$2 $1\n$RESET"
}


##
# Check that the binary is installed otherwise  exit.
#
# Check that the binary command provided is available on the OS where this function is executed.
# if the binary is not installed stop execution.
#
#  $1: the binary linux command to check.
function checkOsBinary(){ 
if [[ $# -ne 1 ]]
  then
    printf "@FuncArgError:${FUNCNAME[0]}:Line=${BASH_LINENO} needs <runnableBinaryName>. Example: ${FUNCNAME[0]} yamllint.\n"
    exit -120
  fi
  local binaryName=$1
  if [[ $(command -v "$binaryName" 2>/dev/null) == "" ]]
  then
    printf "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} a required binary '$binaryName' is not installed on $(hostname) server. Please install it\n" 
    exit -121
  fi
}


##
#
# Check that all script dependencies package are present on the current system otherwise stop and exit. 
function checkDependencies(){
  for pkg in ${DEPENDENCIES_PACKAGES[*]}; do 
    checkOsBinary "$pkg"
  done
}


##
function usage(){
  echo "sudo $(getScriptName) --checkFile=true --vmsDefinitionFileFQP=/papi/infra/data/IncusVmsDefinition.yaml bashVarsPrefix='gfs'" 
}


# Make sure only root can run our script
function checkUserIsRootOrSudoer(){
  if [[ $EUID -ne 0 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} This script must be run as root\n" $RED
    exit -130
  fi
}


# Get script name
function getScriptName(){
  echo "$(readlink -f ${BASH_SOURCE[0]})"
}


#FNCT:960
#  
# Check that bashVarsPrefix '^[a-zA-Z][0-9a-zA-Z_]*$'"
#
#
function checkWord(){
  if [[ $# -ne 1 ]]; then
    printf "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}  ${FUNCNAME[0]} wordToCheck\n"
    exit -150
  fi
  if [[ ! $1 =~ ^[a-zA-Z][0-9a-zA-Z_]* ]] 
  then
    printf "The provided string "$1" is incorrect\n"   
    exit -151
  fi
}


##
# Check than all the arguments provided to the script match the format '--argName=value'
#
function checkAllArgumentsFormat(){
  for arg in ${allArgsProvidedToScript}; do
  local argProvided=$(echo "$arg" | grep -Po "(?<=^\-\-)(\w+)=" | cut -d'=' -f1) 
  if [[  $argProvided == ${EMPTY_STR} ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}. $(getScriptName) script's argument must be probided --\w+=Value\n" $RED
    exit -170
  elif [[ ! " ${allArgNames[*]} " == *" $argProvided "*  ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}. The argument provided $arg is incorrect.\n$(getScriptName) script arguments must on of this:\n${allArgNames[*]}\n" $RED
    exit -170
  fi
  done
}


##
# Get all arguments of the script and check that all the mandatory are present
#
#
function checkMandatoryArgs(){
  for argName in ${mandatoryArgsArray[*]}; do
  if [[ ! "$allArgsProvidedToScript" == *"--$argName="* ]]; then 
      printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} The mandatory argument $argName must be probided to this script as --$argName=\n" $RED
      exit -180
    fi
  done
}


##
# Returns all the optional arguments provided to the script as --optArg1=value1 --optArg2=value2 etc.
#
#
# return: the list of all optional argument provided to the script. 
function getAllOptionalArgumentsProvided(){
  local res=""
  for optArgName in ${optionalArgsNameArray[*]}; do
  if [[ "$allArgsProvidedToScript" == *"--$optArgName="* ]]; then 
    res="$res $optArgName"
  fi
  done
  echo "${res:1}"
}


##
#
# Export the value of an argument from its name. If the argument is optional and was provided to the script, then 
# it's default value is ised as its value
#
function exportAllArguments(){
  checkMandatoryArgs
  checkAllArgumentsFormat
  for arg in ${allArgsProvidedToScript}; do
     local argName=$(echo "$arg"| cut -d'=' -f1) 
     argName=${argName:2}
     export "$argName"=$(cut -d'=' -f2 <<< $arg)
  done
  # Process the optional argument since they are not always provided as script argument
  # So it was not exported from the for loop !!!
  for optArg in ${optionalArgsNameArray[*]}; do
    if [[ $(declare -x | grep -Po "(?<=declare -x )($optArg=)") == ${EMPTY_STR} ]]; then    
      export "$optArg"=${optionalArgsArray[$optArg]}
    fi
  done
}


function checkVmDefinitionFile(){
  if [[ $checkFile == $OK ]]; then
     if [[ ! -f $vmsDefinitionFileFQP ]]; then
        printInputInColor "@Info:$(getScriptName):Line:${BASH_LINENO} There is no existing '${vmsDefinitionFileFQP}' file  on the current host $(hostaname)" $RED
	exit -120
     else
       local res=$(yamllint $vmsDefinitionFileFQP 2>/dev/null)
       if [[ $res != "" ]]; then
        printInputInColor "@Info:$(getScriptName):Line:${BASH_LINENO} Incorrect vms definition yaml file '${vmsDefinitionFileFQP}'" $RED
        printInputInColor "@Error:$(getScriptName): $res" $RED
	exit -121
       fi
     fi
  fi
}


checkBashVarsPrefix(){
 if [[ $bashVarsPrefix != "" ]]; then
    checkWord $bashVarsPrefix
 fi
}


##
#  Get the list of all vm name defined within a vmsDefinitionFileFQP yaml file. 
#
#@return the names of all vms.
function getAllVmNames(){
  echo $(grep -Po  "(?<=^\s{2})(\w+)" VmsDefinitionFile.yml)  
}


##
#  Get the address of ip1 for given vm as "${vmName}@ip1@address ip1Address" from VmsDefinitionFile if it exists  otherwise return empty string "vmName@ip1@address NOT_DEFINED" 
#
# Mandatory Arg:
#  $1: the name of the vm.
#
#@return the string  "${vmName}@ip1@address ip1Address" or "vmName@ip1@address NOT_DEFINED" 
#
function getVmIp1Address(){
   if [[ $# -ne 1 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}  ${FUNCNAME[0]} vmName\n" $RED
    exit -130
  fi
  local vmName="$1"
  checkWord $vmName
  local resPrefix="${vmName}@ip1@address"
  local res="$resPrefix NOT_DEFINED"
  ## If ip1 is defined for the vm it data will be located until 5th line after vmName key: 
  local vmData=$(grep -P -A5 "^\s{2}$vmName:" VmsDefinitionFile.yml)
  local entityCandidate=$(sed '4q;d' <<< $vmData | grep "ip1:")
  if [[  "$entityCandidate" != ${EMPTY_STR} ]]; then
    local entityValue=$(sed '5q;d' <<< $vmData | cut -d':' -f2 | xargs)
    res="$resPrefix $entityValue"
  fi
  echo $res
}


##
#  Get the netType of ip1 for given vm as "${vmName}@ip1@netType ip1NetType" from VmsDefinitionFile if it exists  otherwise return empty string "vmName@ip1@netType NOT_DEFINED" 
#
# Mandatory Arg:
#  $1: the name of the vm.
#
function getVmIp1NetType(){
   if [[ $# -ne 1 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}  ${FUNCNAME[0]} vmName\n" $RED
    exit -130
  fi
  local vmName="$1"
  checkWord $vmName
  local resPrefix="${vmName}@ip1@netType"
  local res="$resPrefix NOT_DEFINED"
  ## If ip1 is defined for the vm it data will be located until 5th line after vmName key: 
  local vmData=$(grep -P -A5 "^\s{2}$vmName:" VmsDefinitionFile.yml)
  local entityCandidate=$(sed '4q;d' <<< $vmData | grep "ip1:")
  if [[  "$entityCandidate" != ${EMPTY_STR} ]]; then
    local entityValue=$(sed '6q;d' <<< $vmData | cut -d':' -f2 | xargs)
    res="$resPrefix $entityValue"
  fi
  echo $res
}


##
#  Get the address of ip2 for given vm as "${vmName}@ip2@address ip2Address" from VmsDefinitionFile if it exists  otherwise return empty string "vmName@ip2@address NOT_DEFINED" 
#
# Mandatory Arg:
#  $1: the name of the vm.
#
function getVmIp2Address(){
   if [[ $# -ne 1 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}  ${FUNCNAME[0]} vmName\n" $RED
    exit -130
  fi
  local vmName="$1"
  checkWord $vmName
  local resPrefix="${vmName}@ip2@address"
  local res="$resPrefix NOT_DEFINED"
  ## If ip2 is defined for the vm it data will be located until 8th line after vmName key: 
  local vmData=$(grep -P -A8 "^\s{2}$vmName:" VmsDefinitionFile.yml)
  local entityCandidate=$(sed '7q;d' <<< $vmData | grep "ip2:")
  if [[  "$entityCandidate" != ${EMPTY_STR} ]]; then
    local entity=$(sed '8q;d' <<< $vmData | cut -d':' -f2 | xargs)
    res="$resPrefix $entity"
  fi
  echo $res
}


##
#  Get the netType of ip2 for given vm as "${vmName}@ip2@netType ip2NetType" from VmsDefinitionFile if it exists  otherwise return empty string "vmName@ip2@netType NOT_DEFINED" 
#
# Mandatory Arg:
#  $1: the name of the vm.
#
function getVmIp2NetType(){
   if [[ $# -ne 1 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}  ${FUNCNAME[0]} vmName\n" $RED
    exit -130
  fi
  local vmName="$1"
  checkWord $vmName
  local resPrefix="${vmName}@ip2@netType"
  local res="$resPrefix NOT_DEFINED"
  ## If ip2 is defined for the vm it data will be located until 8th line after vmName key: 
  local vmData=$(grep -P -A8 "^\s{2}$vmName:" VmsDefinitionFile.yml)
  local entityCandidate=$(sed '7q;d' <<< $vmData | grep "ip2:")
  if [[  "$entityCandidate" != ${EMPTY_STR} ]]; then
    local entity=$(sed '9q;d' <<< $vmData | cut -d':' -f2 | xargs)
    res="$resPrefix $entity"
  fi
  echo $res
}


##
#  Get the type and the description of the vm as "${vmName}@type typeValue" from VmsDefinitionFile or empty string "vmName@type NOT_DEFINED" 
#
# Mandatory Arg:
#  $1: the name of the vm.
#
#@return the string  "${vmName}@type typeValue " or "vmName@type NOT_DEFINED" 
#
function getVmType(){
   if [[ $# -ne 1 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}  ${FUNCNAME[0]} vmName\n" $RED
    exit -130
  fi
  local vmName="$1"
  checkWord $vmName
  local res="${vmName}@type NOT_DEFINED"
  ## If type is defined for the vm it will be at 1th line after vmName key: 
  local vmTypeData=$(grep -P -A1 "^\s{2}$vmName:" VmsDefinitionFile.yml)
  local typeCandidate=$(sed '2q;d' <<< $vmTypeData | grep "type:")
  if [[  "$typeCandidate" != ${EMPTY_STR} ]]; then
    local type=$(sed '2q;d' <<< $vmTypeData | cut -d':' -f2 | xargs)
    res="${vmName}@type $type"
  fi
  echo $res
}


##
#  Get the description of the vm as "${vmName}@description 'descriptionValue'" from VmsDefinitionFile or empty string "vmName@description NOT_DEFINED" 
#
#
# Mandatory Arg:
#  $1: the name of the vm.
#
#@return the string  "${vmName}@description 'typeValue'" or "vmName@description NOT_DEFINED" 
# On notice that the descrition value is simple quoted.
function getVmDescription(){
   if [[ $# -ne 1 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}  ${FUNCNAME[0]} vmName\n" $RED
    exit -130
  fi
  local vmName="$1"
  checkWord $vmName
  local res="${vmName}@description NOT_DEFINED"
  ## If description is defined for the vm it will be at 1th line after vmName key: 
  local vmData=$(grep -P -A2 "^\s{2}$vmName:" VmsDefinitionFile.yml)
  local dataCandidate=$(sed '3q;d' <<< $vmData | grep "description:")
  if [[  "$dataCandidate" != ${EMPTY_STR} ]]; then
    local entity=$(sed '3q;d' <<< $vmData | cut -d':' -f2 | xargs)
    res="${vmName}@description '$entity'"
  fi
  echo $res
}


##
# return all the names of direct keys for all the vms defined within vmsDefinitionFileFQP
#
function getAllVmsDirectChildrenKeyNames(){
  checkVmDefinitionFile
  echo $(grep -Po "(?<=^\s{4})(\w+)" $vmsDefinitionFileFQP | sort -u)
}


##
# Get vm direct children's key value within VmDefinitionFile yaml file from: 
#   - vm name 
#   - vm children key name. 
# 
# Mandatory Arg: 
#   $1: the name of vm for which the key value is to find.
#   $2: the name of vm children's key 
function getVmDirectChildrenKeyValue(){
   if [[ $# -ne 2 ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} Incorrect number of arguments.\nUsage:${FUNCNAME[0]} vmName childrenKey\n" $RED
    exit -160
  fi
  # check provide vm name between all vm names of VmsDefinitionFile
  local vmName=$1
  local childrenKeyName=$2
  local allVms=$(getAllVmNames)
  if [[ ! " $allVms " ==  *" $vmName "* ]]; then
    printInputInColor "@Info:$(getScriptName):Line:${BASH_LINENO} There is no vm named $vmName within '${vmsDefinitionFileFQP}' yam file." $RED
    exit -163
  fi
  local res="${vmName}@${childrenKeyName} NOT_DEFINED"
  ## If children key is defined for the vm it will at max line of 21 after vmName key: 
  ## Remove the other vm properties if the selected 21 lines contains them
  local vmData=$(grep -P -A21 "^\s{2}$vmName:" VmsDefinitionFile.yml | grep -v "$vmName")
  local beforeOtherVmData=$( echo "$vmData"  | grep -B21 -P "^\s{2}\w+:" | grep -vP "^\s{2}\w+:") 
  if [[  ! $beforeOtherVmData == ${EMPTY_STR} ]]; then vmData=$beforeOtherVmData; fi
  local candidate=$(grep -P "^\s{4}${childrenKeyName}:" <<< $vmData)
  if [[ !  "$candidate" == ${EMPTY_STR} ]]; then
    local keyValue=$(cut -d':' -f2 <<<$candidate | xargs)
    res="${vmName}@${childrenKeyName} $keyValue"
  fi
  echo "$res"
}


function parseVmDefinitionFile(){
   for vm  in $(getAllVmNames); do 
     getVmIp1Address $vm
     getVmIp1NetType $vm
     getVmIp2Address $vm
     getVmIp2NetType $vm

    for childrenKeyName in $(getAllVmsDirectChildrenKeyNames); do
      getVmDirectChildrenKeyValue $vm "$childrenKeyName"
    done
  done
}

##
# Export all the VmDefinitionFile entries keys as bash variables prefixed by bashVarsPrefix
#
#
function exportAllParsedVariables(){
#printInputInColor "@Info:$(getScriptName):Line:${BASH_LINENO} Parse "${VM_DEFINTION_DEFAULT_FILE_FQP}" And Export all entries expanded with prefix variable '$bashVarsPrefix'" $GREEN
  local res=$(parseVmDefinitionFile)
  res=${res//@/_}
  while read line; do 
    eval export "${bashVarsPrefix}_${line/ /=}"
  done <<< $res
 }


function createDeclaredVariableShFile(){
  local workdirFQP=/tmp/workdir
  local exportedVariablesShFileName="AllVmsProperties.sh"
  rm -rf $workdirFQP
  mkdir $workdirFQP
  cd $workdirFQP
  export -p | grep "${bashVarsPrefix}_"  > $exportedVariablesShFileName
  printInputInColor "@Info:$(getScriptName):Line:${BASH_LINENO} Parse "${VM_DEFINTION_DEFAULT_FILE_FQP}" And Export all entries expanded with prefix variable '$bashVarsPrefix' into sh file: $workdirFQP/$exportedVariablesShFileName" $GREEN
  chmod +x $exportedVariablesShFileName
}



function main(){
  checkDependencies
  exportAllArguments
  checkVmDefinitionFile
  checkBashVarsPrefix
  exportAllParsedVariables
  createDeclaredVariableShFile
}


## Run main
main
