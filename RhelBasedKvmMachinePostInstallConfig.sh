#!/usr/bin/env bash
#
# Script is used to configure the fresh mewly installed rhel kvm machine. This script is used on the host machine.
# 
# Arguments:
#    vmIp: the ipv4 of the vm  in which remote command will be executed to configure it.
#    vmUsername: the admin username of kvm machine. This user's ssh key was deployed on the kvm machine with cloud-init during its installation
#
# Arguments:
#   Mandatory: 
#    vmIp: the ip of the vm  in which remote command will be executed to configure it.
#    vmUsername: the admin username of kvm machine. This user's ssh key was deployed on the kvm machine with cloud-init during its installation
#
#   Optional:  
#     sshConnectionTimeout: The ssh connection timeout  to server @ address vmIp
#
#  Example of kvm machine configuration 
#    /papi/projects/virshkvm/InstallRhelBasedKvmMachine.sh  --vmIp=172.16.23.17 --vmUsername=vadima
set -eu
set -o pipefail


# Color for consol output
export RED=$(tput setaf 1)
export GREEN=$(tput setaf 2)
export ORANGE=$(tput setaf 3)
export YELLOW=$(tput setaf 3)
export BLUE=$(tput setaf 4)
export MAGENTA=$(tput setaf 5)
export LINE=$(printf '_%.s' {1..120})
export RESET=$(tput sgr0)

## Adipappi infra bash global constant and string
export OK=true
export KO=false
export EMPTY_STR=""
export NOT_YET_SET="NOT_YET_SET"
export IPV4_REGEX="(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])"
export QEMU_MAC_ADDRESS_REGEX="(52:54:00:[0-9a-f][0-9a-f]:[0-9a-f][0-9a-f]:[0-9a-f][0-9a-f])"

## Dependencies 
declare -a DEPENDENCIES_BINARY
declare -a DEPENDENCIES_PACKAGES


## Mandatory Arguments Array
declare -a mandatoryArgsArray=(vmIp vmUsername)

## Different Array or Arguments
declare -A optionalArgsArray=(sshConnectionTimeout)
declare -a optionalArgsNameArray=(${!optionalArgsArray[*]})
 
## Build All Arguments Array
declare -a allArgNames=( ${mandatoryArgsArray[*]} ${!optionalArgsArray[*]} )
## All the arguments provided to the  script 
declare allArgsProvidedToScript="${BASH_ARGV[@]}"


# Get script name
function getScriptName(){
  echo $(basename -- "$BASH_SOURCE")
}

##
# Return the local day in the following format "-u +%Y-%m-%dT%H:%M:%S%Z"
#
function getCurrentLocalDate(){
  date +"%Y-%m-%dT%H:%M:%S%Z"
}

#
# Print the given message in specified color on to terminal
#
# $1: Message to print into console
# $2: Output color of the message on console
#
function printInputInColor(){
  if [[ $# -ne 2 ]]; then
    printf "Usage example: ${FUNCNAME[0]} USAGE_MESSAGE \$YELLOW\n"
    exit -100
  fi
    printf "$2 $1\n$RESET"
}


## Script Usage
function usage(){
  printInputInColor "$(getScriptName) --vmIp=172.16.23.17 --vmUsername=vadima" $RED
  local optsMesg="optional arguments name and default value are"
  local opts=$optsMesg
  for opt in ${!optionalArgsArray[@]}
  do
   opts="$opts [--$opt=${optionalArgsArray[$opt]}]"
  done
  if [[ "$opts" != "$optsMesg" ]] ; then  printInputInColor "$opts" $ORANGE; fi
}



##
#
# return in lowercase the current server distribution name.  
#
# @return: lowercased distro name


##
# Check than all the arguments provided to the script match the format '--argName=value'
#
function checkAllArgumentsFormat(){
  for arg in ${allArgsProvidedToScript}; do
  local argProvided=$(echo "$arg" | grep -Po "(?<=^\-\-)(\w+)=" | cut -d'=' -f1) 
  if [[  $argProvided == ${EMPTY_STR} ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}. $(getScriptName) script's argument must be probided --\w+=Value\n" $RED
    exit -170
  elif [[ ! " ${allArgNames[*]} " == *" $argProvided "*  ]]; then
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO}. The argument provided $arg is incorrect.\n$(getScriptName) script arguments must on of this:\n${allArgNames[*]}\n" $RED
    exit -170
  fi
  done
}


##
# Get all arguments of the script and check that all the mandatory are present
#
#
function checkMandatoryArgs(){
  for argName in ${mandatoryArgsArray[*]}; do
  if [[ ! "$allArgsProvidedToScript" == *"--$argName="* ]]; then 
    printInputInColor "Error@${FUNCNAME[0]}:Line=${BASH_LINENO} The mandatory argument $argName must be probided to this script as --$argName=<argValue>\n" $RED
    usage
      exit -180
    fi
  done
}


##
# Returns all the optional arguments provided to the script as --optArg1=value1 --optArg2=value2 etc.
#
#
# return: the list of all optional argument provided to the script. 
function getAllOptionalArgumentsProvided(){
  local res=""
  for optArgName in ${optionalArgsNameArray[*]}; do
  if [[ "$allArgsProvidedToScript" == *"--$optArgName="* ]]; then 
    res="$res $optArgName"
  fi
  done
  echo "${res:1}"
}


##
#
# Export the value of an argument from its name. If the argument is optional and was provided to the script, then 
# it's default value is ised as its value
#
function exportAllArguments(){
  for arg in ${allArgsProvidedToScript}; do
     local argName=$(echo "$arg"| cut -d'=' -f1) 
     argName=${argName:2}
     export "$argName"=$(cut -d'=' -f2 <<< $arg)
  done
  # Process the optional argument since they are not always provided as script argument
  # So it was not exported from the for loop !!!
  for optArg in ${optionalArgsNameArray[*]}; do
    if [[ $(declare -x | grep -Po "(?<=declare -x )($optArg=)") == ${EMPTY_STR} ]]; then
      export "$optArg"=${optionalArgsArray[$optArg]}
    fi
  done
}


##
# Check vmIp against IPV4_REGEX
function checkVmIp(){
  if [[ $(echo $vmIp | grep -P "${IPV4_REGEX}" ) == ${EMPTY_STR} ]]; then
    printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} the provided ip $vmIp is not a valid ip address" $RED
    exit -209
  fi
}


##
#
# Check the username of the vm against \w+
function checkVmUsername(){
  if [[ $(echo $vmUsername | grep -P "^[a-z][-a-z0-9_]*\$" 2>/dev/null) == ${EMPTY_STR} ]]; then
    printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} username '$vmUsername' provided for the vm @ip address '$vmIp'"  $RED
    exit -220
  fi
}


##
# check that sshConnectionTimeout is integer
#
function checkSshConnectionTimeout(){
  if [[ $sshConnectionTimeout =~ '^[1-9][0-9]+$' ]]; then
    printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} '$sshConnectionTimeout' is an incorrect ssh connection timeout value."  $RED
    exit -230
  fi
}


##
#
# Function used to call all others chercker functions. 
#
function checkScriptDataExportArguments(){
  checkAllArgumentsFormat
  checkMandatoryArgs
  exportAllArguments
  checkVmIp
  checkVmUsername
  checkSshConnectionTimeout
}


## 
#
# Delete hanged dnf and Configure the resolv.conf
#
# @todo create ssh runner script.
function configureVm() {
  local resolvConfFileFQP=/etc/resolv.conf
  local sshPrefix="ssh -tt -o StrictHostKeyChecking=no -o ConnectTimeout=$sshConnectionTimeout -o BatchMode=yes $vmUsername@$vmIp"
  local res=$($sshPrefix "sudo kill -9 \$(ps -def | awk '/\/usr\/bin\/dnf/ {print \$3}') 2>/dev/null" 2>&1)
  if  [[ "$res" != ${EMPTY_STR} ]]; then
    printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} Ssh connection to vm at ip address '$vmIp' is impossible.\nCheck that vm ip address $vmIp is correct"  $ORANGE
    exit -240
  fi
  local res=$($sshPrefix "echo -e 'nameserver 1.1.1.1\nnameserver 8.8.4.4' | sudo tee $resolvConfFileFQP; sudo chattr +i $resolvConfFileFQP" 2>&1)
 if  [[ "$res" != ${EMPTY_STR} ]]; then
    printInputInColor "@Error:${FUNCNAME[0]}:Line=${BASH_LINENO} Ssh connection to vm at ip address '$vmIp' is impossible.\nCheck that vm ip address $vmIp is correct"  $ORANGE
    exit -250
  fi
}


function main(){
  checkScriptDataExportArguments
  configureVm
}


## Run main

main


