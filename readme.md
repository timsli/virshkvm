# User libvirt and virsh to create and manage kvm machine
Create vm by using InstallRhelBasedKvmMachine.sh script with depends on cloud-init. The public ssh key of the current user who run the script is deployed on the kvm vm to ensure that he can connect to the vm after the creation. 


# Call the script used to parse the file VmsDefinitionFile.yml
 @Info:/papi/projects/virshkvm/ParseVmDefinitionFile.sh:Line:521 Parse ./VmsDefinitionFile.yml And Export all entries expanded with prefix variable 'all_vms' into sh file: /tmp/workdir/AllVmsProperties.sh

When on executes the script ParseVmDefinitionFile.sh  all the bash variables created are stored into the sh file: */tmp/workdir/AllVmsProperties.sh*


# Create a rhel based kvm vm machine on ubuntu host. The machine can go to internet by using wifi network.
sudo /papi/projects/virshkvm/InstallRhelBasedKvmMachine.sh \
--vmName=vstrglnx1 \
--vcpu=2 \
--ramMiB=2048 \
--vmUsername=vadima \
--vmGroupname=adimida \
--sshPublicKeyFQP=/home/timsli/.ssh/id_ed25519.pub \
--deleteVmIfExists=true \
--hostNicName=virbr0 \
--kvmNatNetworkName=kvmnatbridge0  \
--createLegacyBridgeNicName=true \
--forwardKvmNetworkToHostWifi=true


#  Example of creating a kvm vm without any access to internet.
sudo /papi/projects/virshkvm/InstallRhelBasedKvmMachine.sh \
--vmName=vstrglnx2 \
--vcpu=2 \
--ramMiB=2048 \
--vmUsername=vadima \
--vmGroupname=adimida \
--sshPublicKeyFQP=/home/timsli/.ssh/id_ed25519.pub \
--deleteVmIfExists=true \
--hostNicName=virbr0 \
--kvmNatNetworkName=kvmnatbridge0  \
--createLegacyBridgeNicName=true \
--forwardKvmNetworkToHostWifi=false


# Configure the created vm from it's ip and username on the host machine.
virsh  net-dhcp-leases --network kvmnatbridge0  | awk 'NR==3 {print $5}'
/papi/projects/virshkvm/RhelBasedKvmMachinePostInstallConfig.sh 172.23.1.195 vadima

